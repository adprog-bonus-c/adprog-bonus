package bot.echo;

import bot.AbstractBot;
import bot.Handlerable;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

public class Echo<T extends AbstractBot> implements Handlerable<T> {
    public synchronized boolean checkIfForMe(Update update) {
        // Echo meng-klaim pesan itu jika pesan itu sama dengan "help echo"
        // atau dimulai dengan "/echo"
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase("/help echo") || (text.toLowerCase()).startsWith("/echo")) {
                return true;
            }
        }
        return false;
    }

    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        if (text.equalsIgnoreCase("/help echo")) {
            message.setText("Format: /echo <argumen>\n" +
                    "Fitur echo mengembalikan <argumen> kembali\n" +
                    "Contoh: /echo Halo akan direspon oleh bot dengan 'Halo!'");
        } else {
            String[] argumen = text.split(" ");
            if (argumen.length == 1) {
                message.setText("Argumen /echo gagal. Dia membutuhkan setidaknya" +
                        " satu argumen");
            } else {
                String hasil = argumen[1];
                for (int I = 2; I < argumen.length; I++) {
                    hasil += " " + argumen[I];
                }
                message.setText(hasil);
            }
        }
        object.send(message);
    }
}

package bot;

import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;

public interface Handlerable<T extends TelegramLongPollingBot> {
    /**
     * <p>Menentukan apakah pesan ini dimaksudkan untuk diproses of fitur (handler) anda</p>
     * <p>NOTES</p>
     * <p>1. Jika pesan itu jelas dimaksudkan untuk anda tapi format data itu tidak valid,
     * checkIfForMe() harus meng-return true. Ketika pesan itu masuk ke processMessage, fungsi anda baru
     * akan memberikan pesan error ke pengguna</p>
     * <p>Sebagai contoh: Jika anda membuat fungsi yaitu \random (range awal) (range akhir) (ie. \random 3 5)
     * tetapi user memberikan invalid (ie. hanya `\random 3) maka fungsi checkIfForMe() harus meng-return true
     * untuk fungsi tersebut tetapi ketika pesan itu dilempar ke processMessage, dia harus memberikan pesan error
     * kepada user</p>
     * <p>2. Implementasi checkIfForMe harus thread-safe. Karena constrain java, tidak bisa langsung
     * methodnya synchronized dari interface.</p>
     * @param   update Pesan tersebut
     * @return sebuah
     */
    public boolean checkIfForMe(Update update);

    /**
     * Memproses pesan tersebut dan meng-generate respon yang sesuai untuk pesan itu.
     * <p>NOTES</p>
     * <p>1. Implementasi checkIfForMe harus thread-safe. Karena constrain java, tidak bisa langsung
     * methodnya synchronized dari interface.</p>
     * @param   update Sebuah pesan yang oleh implementasi checkIfForMe() dari kelas anda meng-return true
     * @param   object Sebuah object yang meng-extend TelegramLongPollingBot.
     */
    public void processMessage(Update update, T object);
}

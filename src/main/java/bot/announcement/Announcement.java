package bot.announcement;

import bot.AbstractBot;
import bot.Handlerable;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

/**
 * Created by Arga Ghulam Ahmad
 *
 * Announcement bot handles user command to give information about Scele Fasilkom UI's announcement.
 * @param <T>
 */
public class Announcement<T extends AbstractBot> implements Handlerable<T> {
    /**
     * Check is that message for announcement bot?
     *
     * @param   update Pesan tersebut
     * @return boolean for me
     */
    public synchronized boolean checkIfForMe(Update update) {
        // Announcement meng-klaim pesan itu jika pesan itu sama dengan "help announcement"
        // atau dimulai dengan "\announcement"
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            return text.equals("/help announcement") || text.startsWith("/announcement");
        }
        return false;
    }

    /**
     * Process message and send message contains information about Scele Fasilkom's Announcement.
     * @param   update Sebuah pesan yang oleh implementasi checkIfForMe() dari kelas anda meng-return true
     * @param   object Sebuah object yang meng-extend TelegramLongPollingBot.
     */
    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        if (text.equals("/help announcement")) {
            message.setText("Format: /announcement <optional: jumlah yang ditampilkan min 1, max 5, dan default 1>\n" +
                    "Fitur announcement mengembalikan pengumuman terbaru yang ada di Scele Fasilkom Universitas Indonesia.\n");
        } else {
            AnnouncementGenerator announcementGenerator = new AnnouncementGenerator();
            String[] argumen = text.split(" ");
            if (argumen.length == 1) {
                String announcements = announcementGenerator.generateAnnouncement(1);
                message.setText(announcements);
            } else {
                int totalOfPost = Integer.parseInt(argumen[1]);
                if (totalOfPost >= 1 && totalOfPost <= 5) {
                    String announcements = announcementGenerator.generateAnnouncement(totalOfPost);
                    message.setText(announcements);
                } else {
                    message.setText("Maaf, angka yang kamu masukkan harus diantara 1 dan 5 inklusif.");
                }
            }
        }
        object.send(message);
    }


}
package bot.announcement;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AnnouncementGenerator {
    private static final String SCELE_CS_UI_URL = "https://scele.cs.ui.ac.id/";

    /**
     * Generate string contains announcement.
     * @param numberOfPosts number of posts.
     * @return String of announcements.
     */
    public String generateAnnouncement(int numberOfPosts) {
        try {
            Document doc = Jsoup.connect(SCELE_CS_UI_URL).get();

            StringBuilder stringBuilder = new StringBuilder();

            String title = doc.title();
            stringBuilder.append("Pengumuman Akademis: ").append(title).append("\n");

            Elements latestNews = doc.select("[class*=forumpost clearfix firstpost starter]");
            stringBuilder.append("-----------------------------------------------------------------------");
            for (int i = 0; i < numberOfPosts; i++) {
                Element news = latestNews.get(i);
                stringBuilder.append(Util.unescapeHTML(Util.removeHtmlTag(news.select("[class*=topic firstpost starter").toString())));
                stringBuilder.append(Util.unescapeHTML(Util.removeHtmlTag(news.select("[class*=posting fullpost]").toString())));
                stringBuilder.append("-/----------------------------/-");
            }
            return stringBuilder.toString();
        } catch (Exception e) {
            return "Maaf, kami belum bisa mengambil pengumuman akademis. Silahkan coba beberapa saat lagi.";
        }
    }
}

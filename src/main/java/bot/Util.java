package bot;

import org.jetbrains.annotations.Nullable;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class Util {

    @Nullable
    public static synchronized Object makeCall(String url) {
        try {
            URL obj = new URL(url);


            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setDoInput(true);
            con.setRequestMethod("GET");

            StringBuffer response = new StringBuffer();

            // Kredensial invalid tidak akan mendapat respons 200
            if (con.getResponseCode() == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONParser parser = new JSONParser();
                Object jsonObject = parser.parse(response.toString());

                return jsonObject;
            } else {
                System.out.println(con.getResponseCode());
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}


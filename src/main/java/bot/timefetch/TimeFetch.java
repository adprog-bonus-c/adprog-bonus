package bot.timefetch;

import bot.AbstractBot;
import bot.Handlerable;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

public class TimeFetch<T extends AbstractBot> implements Handlerable<T> {

    private final boolean FORMAT_IN_DATE = true;

    @Override
    public synchronized boolean checkIfForMe(Update update) {
        // Echo meng-klaim pesan itu jika pesan itu sama dengan "help echo"
        // atau dimulai dengan "\echo"
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase("/help waktu") || (text.toLowerCase()).startsWith("/waktu")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        String replyText = null;
        if (text.equalsIgnoreCase("/help waktu")) {
            message.setText("Format: /waktu\n" +
                    "Mengembalikan waktu server scele\n" +
                    "Contoh: /waktu akan mengambilkan tanggal dan waktu saat ini");
            object.send(message);
            return;
        } else if(text.equalsIgnoreCase("/waktu")) {
            replyText = TimeFetcher.getTime(TimeFetcher.getTimeServer(), FORMAT_IN_DATE);
        } else if(text.startsWith("/waktu")){
            replyText = "Argumen /waktu tidak butuh argumen";
        }
        message.setText(replyText);
        object.send(message);
    }


}

package bot.trainschedule;

import bot.AbstractBot;
import bot.Authorization;
import bot.Handlerable;

import bot.timefetch.TimeFetcher;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.charset.Charset;

public class TrainSchedule<T extends AbstractBot> implements Handlerable<T> {
 
    protected String API_URL = "https://maps.googleapis.com/maps/api/directions/json?";
    private final boolean FORMAT_IN_SECS = false;

    @Override
    public synchronized boolean checkIfForMe(Update update) {
        // Echo meng-klaim pesan itu jika pesan itu sama dengan "help echo"
        // atau dimulai dengan "\echo"
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase("/help kereta") || (text.toLowerCase()).startsWith("/kereta")) {
                return true;
            }
        }
        return false;
    }

    @Override
    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText().trim().toLowerCase();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        String replyText = null;
        if (text.equalsIgnoreCase("/help kereta")) {
            message.setText("Format: /kereta <stasiun awal> - <stasiun tujuan>\n" +
                    "Mengembalikan jadwal kereta dari stasiun awal ke stasiun tujuan\n" +
                    "Contoh: /kereta stasiun manggarai - stasiun depok\n"+
                    "akan mengambilkan jadwal KRL Manggarai-Depok");
            object.send(message);
            return;
        } else if(text.equalsIgnoreCase("/kereta")) {
            replyText = "/kereta membutuhkan argumen <stasiun awal> dan <stasiun tujuan>";
        } else if(text.startsWith("/kereta")){
            text = text.substring(7).trim();
            String[] input = text.split("-");
            if(input.length==2) {
                String dateInSecs = TimeFetcher.getTime(TimeFetcher.getTimeServer(), FORMAT_IN_SECS);
                String stasiunAwal = input[0].trim();
                String stasiunTujuan = input[1].trim();
                replyText = getSchedule(dateInSecs,stasiunAwal,stasiunTujuan);
            } else {
                replyText = "Input stasiun tidak sesuai";
            }
        }
        message.setText(replyText);
        object.send(message);
    }

    public synchronized String getSchedule(String seconds, String stasiunAwal, String stasiunTujuan) {
        String awal = stasiunAwal.replace(" ","+");
        String tujuan = stasiunTujuan.replace(" ","+");
        String url = API_URL + "origin=" + awal + "&destination=" + tujuan + "&mode=transit&transit_mode=train" +
                "&departure_time=" + seconds + "&key=" + Authorization.getGoogleMapsAPI();
        try {
            String data = readJSONFromURL(url).toString();
            JSONObject json = new JSONObject(data.trim());
            String status = json.get("status").toString();
            String text = "Stasiun tidak ditemukan";
            if(status.equalsIgnoreCase("OK")) {
                String routesString = json.getJSONArray("routes").toString();
                routesString = routesString.substring(1,routesString.length()-1);
                JSONObject routes = new JSONObject(routesString);
                JSONObject fare = routes.getJSONObject("fare");
                String legsString = routes.getJSONArray("legs").toString();
                legsString = legsString.substring(1,legsString.length()-1);
                JSONObject legs = new JSONObject(legsString);
                JSONObject arrival = legs.getJSONObject("arrival_time");
                JSONObject departure = legs.getJSONObject("departure_time");
                String harga = fare.get("text").toString();
                String keberangkatan = departure.get("text").toString();
                String kedatangan = arrival.get("text").toString();
                text = "Keberangkatan dari " + stasiunAwal + " pada pukul " + keberangkatan +
                        " tiba di " + stasiunTujuan + " pada pukul " + kedatangan + " dengan tarif " + harga;
            }
            return text;
        } catch (IOException e){
            return "Invalid URL";
        }
    }

    public synchronized JSONObject readJSONFromURL(String url) throws IOException {
        InputStream data = new URL(url).openStream();
        JSONObject json;
        try {
            BufferedReader rd = new BufferedReader(new InputStreamReader(data, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            json = new JSONObject(jsonText);
        } finally {
            data.close();
        }
        return json;
    }

    private synchronized String readAll(BufferedReader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }
}

package bot.jadwal;

import bot.AbstractBot;
import bot.Authorization;
import bot.Handlerable;
import bot.Util;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.JSONParser;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

import static org.apache.commons.lang3.StringUtils.isNumeric;

public class Jadwal <T extends AbstractBot> implements Handlerable<T> {

    private Long tahun;
    private Long term;

    public synchronized boolean checkIfForMe(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            if (text.equalsIgnoreCase("/help jadwal") || (text.toLowerCase()).startsWith("/jadwal")) {
                return true;
            }
        }
        return false;
    }

    public synchronized boolean fetchTahunAndTerm(String accessToken, String clientId){
        if(tahun != null && term != null){
            return true;
        }

        String url = "https://api.cs.ui.ac.id/siakngcs/periode/get-current-active/?client_id="
                + clientId + "&access_token=" + accessToken + "&format=json";

        JSONObject objek = (JSONObject) Util.makeCall(url);
        if(objek == null){
            return false;
        }
        tahun = (Long) objek.get("tahun");
        term = (Long) objek.get("term");
        return true;
     }


    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);

        if (text.equalsIgnoreCase("/help jadwal")) {
            message.setText("Format: /jadwal <npm>\n" +
                    "Mengembalikan jadwal untuk <npm> untuk masa pelajaran sekarang\n" +
                    "Contoh: /jadwal 1606881670 akan mengambilkan jadwal untuk mahasiswa tersebut");
            object.send(message);
            return;
        }
        String[] args = text.split(" ");

        if (args.length == 1){
            message.setText("Argumen npm membutuhkan satu argumen tambahan yaitu NPM");
            object.send(message);
            return;
        }

        String npm = args[1];

        if(!isNumeric(npm)){
            message.setText(npm+" bukanlah NPM yang valid");
            object.send(message);
            return;
        }

        String accessToken =  Authorization.getAccessCode();
        if(accessToken == null) {
            message.setText("Beep bop. Terjadi kesalahan teknis. Mohon mencoba lagi.");
            object.send(message);
            return;
        }

        String clientId = Authorization.getClientId();

        if(accessToken == null){
            message.setText("Beep bop. Terjadi kesalahan teknis. Mohon mencoba lagi.");
            object.send(message);
            return;
        }

        boolean fetch = fetchTahunAndTerm(accessToken,clientId);

        if(!fetch){
            message.setText("Beep bop. Terjadi kesalahan teknis. Mohon mencoba lagi.");
            object.send(message);
            return;
        }

        // Validasi bahwa NPM tersebut valid
        String url = String.format("https://api.cs.ui.ac.id/siakngcs/mahasiswa/cari-info-program/" +
                "%s/?access_token=%s&client_id=%s&format=json" , npm, accessToken, clientId);

        Object result = Util.makeCall(url);

        if(result == null){
            message.setText("Beep bop. Bot mengalami kesulitan dalam memproses NPM. Ini mungkin terjadi karena" +
                    " kesalahan teknis atau NPM anda yang salah. Mohon mencoba lagi.");
            object.send(message);
            return;
        }

        message.setText("Jadwal untuk NPM ("+npm+") sedang diproses. Ini akan membutuhkan beberapa " +
                "detik untuk diproses.");
        object.send(message);

        StringBuilder jadwal = new StringBuilder();

        String[] daftarHari = {"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu"};

        SendMessage message2 = new SendMessage().setChatId(chatId);

        for(String hari : daftarHari) {
            jadwal.append("~~" + hari + "~~\n");
            url = String.format("https://api.cs.ui.ac.id/siakngcs/jadwal-list/%s/%s/" +
                    "%s/%s/?client_id=%s&access_token=%s&format=json", tahun, term, hari, npm, clientId, accessToken);
            JSONArray daftarJadwal = (JSONArray) Util.makeCall(url);
            if(jadwal==null) {
                message2.setText("Beep bop. Terjadi kesalahan teknis dari server saat mengambil" +
                        " jadwal. Mohon mencoba lagi.");
                object.send(message2);
                return;
            }
            else if (daftarJadwal.size() == 0) {
                jadwal.append("Kosong\n");
            } else {
                for (int I = 0; I < daftarJadwal.size(); I++) {
                    JSONObject item = (JSONObject) daftarJadwal.get(I);
                    JSONObject kelas = (JSONObject) item.get("kd_kls_sc");
                    String namaKelas = (String) kelas.get("nm_kls");
                    JSONObject ruang = (JSONObject) item.get("id_ruang");
                    String namaRuang = (String) ruang.get("nm_ruang");
                    String waktuAwal = (String) item.get("jam_mulai");
                    String waktuAkhir = (String) item.get("jam_selesai");
                    jadwal.append(String.format("%s %s-%s @ %s\n", namaKelas, waktuAwal, waktuAkhir, namaRuang));

                }
            }
        }
        message2.setText(jadwal.toString());
        object.send(message2);
    }
}

package bot.weather;

import bot.AbstractBot;
import bot.Handlerable;
import org.jetbrains.annotations.NotNull;
import org.json.*;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;


public class Weather<T extends AbstractBot> implements Handlerable<T> {

    private Long tahun;
    private Long term;
    private final String[] LOC_OPT = new String[]{"depok", "salemba"};
    private final String[] LOC_COORD = new String[]{"lat=-6.3678578&lon=106.825158", "lat=-6.1936972&lon=106.8490886"};
    private final String URL = "https://api.openweathermap.org/data/2.5/";
    private final String TIME_FORMAT = "EEE MMM dd HH:mm:ss z yyyy";
    private static String WEATHER_TOKEN = System.getenv("WEATHER_TOKEN");

    public synchronized boolean checkIfForMe(Update update) {
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText().trim();
            if (text.equalsIgnoreCase("/help cuaca") || (text.toLowerCase()).startsWith("/cuaca")) {
                return true;
            }
        }
        return false;
    }


    /*
        Entah kenapa tidak bisa pakai yang di Util.

        Entahlah, tanya Javasus Christ.
     */
    public synchronized JSONObject makeCall(String url) {
        try {
            URL obj = new URL(url);

            HttpsURLConnection con = (HttpsURLConnection) obj.openConnection();
            con.setDoInput(true);
            con.setRequestMethod("GET");

            StringBuffer response = new StringBuffer();

            // Kredensial invalid tidak akan mendapat respons 200
            if (con.getResponseCode() == 200) {
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                while ((inputLine = in.readLine()) != null) {
                    response.append(inputLine);
                }
                in.close();

                JSONObject jsonObject = new JSONObject(response.toString());

                return jsonObject;
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText().trim().toLowerCase();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        String param = text.substring(6).trim();
        if (text.equalsIgnoreCase("/help cuaca")) {
            message.setText("Format: /cuaca <lokasi>\n" +
                    "default dari lokasi adalah depok\n" +
                    "Pilihan yang bisa digunakan adalah: depok, salemba");
        } else if (text.equalsIgnoreCase("/cuaca")) {
            message.setText(fetchWeather(0, "weather", URL));
        } else if (Arrays.stream(LOC_OPT).anyMatch(param::equals)) {
            message.setText(fetchWeather(Arrays.asList(LOC_OPT).indexOf(param), "weather", URL));
        } else {
            message.setText("Parameter lokasi tidak ditemukan\n silakan lihat opsi di /help cuaca\n");
        }
        object.send(message);

    }

    public synchronized String fetchWeather(int param, String type, String url) {
        if (type == "weather") {
            try {
                String tempURL = url + "weather?&lang=id&" + LOC_COORD[param] + "&appid=" + WEATHER_TOKEN;
                JSONObject objek = makeCall(tempURL);
                return mapWeathertoString(mapWeatherAPItoArray(objek));
            } catch (Exception e) {
                return "API Bermasalah";
            }
        } else {
            return "Opsi salah";
        }
    }

    /*String Builder untuk map array ke string
     */
    public synchronized String mapWeathertoString(String[] hasil) {
        StringBuilder sb = new StringBuilder();
        sb.append("Cuaca ");
        sb.append(hasil[7]);
        sb.append("\nWaktu : ");
        sb.append(convertToWIB(new Date(Long.parseLong(hasil[1]) * 1000)));
        sb.append("\nKelembaban : ");
        sb.append(hasil[2]);
        sb.append("%\nTekanan : ");
        sb.append(hasil[3]);
        sb.append(" hPa\nTemperatur (sekarang,max,min) : ");
        sb.append(hasil[4]);
        sb.append("°C,");
        sb.append(hasil[5]);
        sb.append("°C,");
        sb.append(hasil[6]);
        sb.append("°C\nWaktu Matahari terbit : ");
        sb.append(hasil[8]);
        sb.append("\nWaktu Matahari terbenam : ");
        sb.append(hasil[9]);
        sb.append("\nKondisi cuaca : ");
        sb.append(hasil[10]);
        sb.append("\nAngin : ");
        sb.append(hasil[12]);
        sb.append(" Kmh");
        if(hasil[11]!=null){
            sb.append(", ");
            sb.append(hasil[11]);
            sb.append("°");
        }
        return sb.toString();
    }

    /*Method untuk menserialize Json object dari weather data ke array -> agar mudah di cache
     *
     * */
    public synchronized String[] mapWeatherAPItoArray(JSONObject obj) {
        String[] serialized = new String[]{
                //0:Method Version,
                "0.0.1",
                //1:time
                Long.toString(obj.getLong("dt")),
                //2:humidity
                Integer.toString(obj.getJSONObject("main").getInt("humidity")),
                //3:pressure  +hPa
                Integer.toString(obj.getJSONObject("main").getInt("pressure")),
                //4:temp
                Integer.toString(obj.getJSONObject("main").getInt("temp") - 273),
                //5:temp max
                Integer.toString(obj.getJSONObject("main").getInt("temp_max") - 273),
                //6:temp min
                Integer.toString(obj.getJSONObject("main").getInt("temp_min") - 273),
                //7:name
                obj.getString("name"),
                //8:sunrise
                convertToWIBShort(new Date(obj.getJSONObject("sys").getLong("sunrise") * 1000)),
                //9:sunset
                convertToWIBShort(new Date(obj.getJSONObject("sys").getLong("sunset") * 1000)),
                //10:deskripsi
                obj.getJSONArray("weather").getJSONObject(0).getString("description"),
                //11:Degree
                failSafeAPI(obj.getJSONObject("wind"),"deg"),
                //12:speed(km)
                Integer.toString(obj.getJSONObject("wind").getInt("speed"))

        };
        return serialized;
    }

    @NotNull
    public synchronized String convertToWIBShort(Date date) {
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss z");
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        return formatter.format(date);
    }

    public synchronized String convertToWIB(Date date) {
        DateFormat formatter = new SimpleDateFormat(TIME_FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Jakarta"));
        return formatter.format(date);
    }

    public synchronized boolean checkIfKeyExist(JSONObject obj, String key){
        if( obj.has(key)){
            return true;
        }else{
            return false;
        }
    }
    /**
     * This class handle inconsistency in API
     */

    public synchronized String failSafeAPI(JSONObject obj, String key){
        if(checkIfKeyExist(obj,key)){
            if(key=="deg"){
                return Integer.toString(obj.getInt(key));
            }else{
                return obj.getString(key);
            }
        }else{
            return null;
        }

    }
}

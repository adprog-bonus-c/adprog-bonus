package bot;

import bot.announcement.Announcement;
import bot.echo.Echo;
import bot.help.Help;
import bot.jadwal.Jadwal;
import bot.timefetch.TimeFetch;
import bot.trainschedule.TrainSchedule;
import bot.weather.Weather;
import bot.help.Help;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.exceptions.TelegramApiException;;
import java.util.ArrayList;

public class AmazingBot extends AbstractBot {

    ArrayList<Handlerable> listOfHandler = new ArrayList<Handlerable>();

    public AmazingBot(boolean mode){
        super(mode);
        listOfHandler.add(new Echo());
        listOfHandler.add(new TimeFetch());
        listOfHandler.add(new Jadwal());
        listOfHandler.add(new Announcement());
        listOfHandler.add(new Weather());
        listOfHandler.add(new Help());
        listOfHandler.add(new TrainSchedule());
    }

    public void onUpdateReceived(Update update) {

        // Pertama-tama, kita mencari fitur mana yang mengklaim bahwa pesan ini
        // dihandle oleh implementasi dia (ie. Design Pattern Chain of Command)

        for(Handlerable handle : listOfHandler){
            if(handle.checkIfForMe(update)){
                // Jika handle meng-klaim pesan ini untuk dia, kita assign fitur ini ke dia
                handle.processMessage(update,this);
                return;
            }
        }

        // Pesan tersebut tidak ada yang mengklaim. Artinya pesan itu memiliki format yang salah dan tidak
        // dimengerti oleh program
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage() // Create a message object object
                .setChatId(chatId)
                .setText("Pesan tidak dimengerti oleh program");
        try {
            execute(message);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    /**
     * Jika usernamenya @MyAmazingBot, hasil kembalian juga harus "MyAmazingBot"
     * @return  String berupa username bot
     */

    public String getBotUsername() { return Authorization.getAccessTokenTelegram(); }

    /**
     * Mengembalikan token untuk BotFather
     * @return  String berupa access token bot
     */
    @Override
    public String getBotToken() { return Authorization.getAccessTokenTelegram(); }
}
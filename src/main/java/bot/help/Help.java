package bot.help;

import bot.AbstractBot;
import bot.Handlerable;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.util.Arrays;

public class Help<T extends AbstractBot> implements Handlerable<T> {

    private final String[] values = {"announcement", "echo", "jadwal", "waktu", "login", "cuaca", "login", "kereta" };

    public synchronized boolean checkIfForMe(Update update) {
        // Echo meng-klaim pesan itu jika pesan itu sama dengan "help echo"
        // atau dimulai dengan "/echo"
        if (update.hasMessage() && update.getMessage().hasText()) {
            String text = update.getMessage().getText();
            // Example : " /hElp "
            String command = text.trim().toLowerCase();
            if (command.equalsIgnoreCase("/help")) {
                return true;
            }else if(command.startsWith("/help")){
                String param = command.substring(5).trim();
                return !Arrays.stream(values).anyMatch(param::equals);
            }
        }
        return false;
    }

    public synchronized void processMessage(Update update, T object) {
        String text = update.getMessage().getText();
        long chatId = update.getMessage().getChatId();
        SendMessage message = new SendMessage().setChatId(chatId);
        if (text.trim().equalsIgnoreCase("/help")) {
            // Setiap fitur akan di list dan menjelaskan perintahnya dalam 1 baris -> pisah dengan \n
            // struct : /perintah penjelasan \n penjelasan(cont.)(kalau ada dan perlu)
            // fitur diurutkan dari a->z
            message.setText("/announcement bot akan menampilkan pengumuman di scele.cs.ui.ac.id\n" +
                    "/cuaca untuk menampilkan cuaca di depok dan salemba\n"+
                    "/echo bot akan mengirimkan kembali pesan yg dikirim\n" +
                    "/help menampilkan bantuan\n" +
                    "/waktu menampilkan waktu\n"+
                    "/kereta menampilkan jadwal kereta");
            object.send(message);
        }else{
            //param
            String param = text.trim().substring(5).trim().toLowerCase();
            message.setText("Bot ini tidak memiliki entri help untuk perintah "+param);
        }

    }
}

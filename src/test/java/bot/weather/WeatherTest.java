
package bot.weather;

import bot.AmazingBot;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONObject;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import java.util.Arrays;
import java.util.Date;

import static org.junit.Assert.*;

public class WeatherTest {
    public Update getUpdateGivenJson(String update) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        Weather weather = new Weather();

        // Tes 1: "/cuaca" -> True

        //Untuk mengerti ini, anda harus reverse-engineering class.
        //Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
        //objek yang ingin reverse-engineer lalu ctrl+alt+D
        // Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
        // Objek Update ada satu kolumn yang ingin kita yaitu column `message`
        // Column `mssage` menampung objek Message yang punya dua atribut
        // - text yaitu '`echo hi!`
        // - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.

        String entry = "{\"message\":{\"text\":\"/cuaca\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(weather.checkIfForMe(upd));

        // Tes 2: "/help cuaca" -> True

        entry = "{\"message\":{\"text\":\"/help cuaca\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(weather.checkIfForMe(upd));


        entry = "{\"message\":{\"text\":\"/help\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!weather.checkIfForMe(upd));
    }

    @Test
    public void validateProcessMessage() {

        // Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
        // Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
        // Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
        // dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
        // package yang sudah ada dan masuk ke testingLog. String ini
        // yang bisa anda periksa kebenarannya
        AmazingBot bot = new AmazingBot(true);
        Weather weather = new Weather();

        // Tes 1 : "\help cuaca mengeluarkan syntax untuk command cuaca
        String entry = "{\"message\":{\"text\":\"/help cuaca\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        weather.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size() == 1);
        assertTrue(bot.testingLog.get(0).contains("Format: /cuaca <lokasi>\n" +
                "default dari lokasi adalah depok\n" +
                "Pilihan yang bisa digunakan adalah: depok, salemba"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));


        // Tes 2 : "/cuaca di-reply tanpa parameter
        entry = "{\"message\":{\"text\":\"/cuaca\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        weather.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size() == 1);
        assertTrue(bot.testingLog.get(0).contains("Cuaca"));

        // Tes 3 : "/cuaca depok
        entry = "{\"message\":{\"text\":\"/cuaca depok\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        weather.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size() == 1);
        assertTrue(bot.testingLog.get(0).contains("Cuaca"));

        // Tes 4 : "/cuaca bandung
        entry = "{\"message\":{\"text\":\"/cuaca bandung\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        weather.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size() == 1);
        assertTrue(bot.testingLog.get(0).contains("Parameter lokasi tidak ditemukan\n silakan lihat opsi di /help cuaca\n"));


    }

    @Test
    public void checkTimeFormat() {
        Weather weather = new Weather();
        assertTrue(weather.convertToWIB(new Date((long) 1525244400 * (long) 1000)).equals("Wed May 02 14:00:00 WIB 2018"));
        assertTrue(weather.convertToWIBShort(new Date((long) 1525244400 * (long) 1000)).equals("14:00:00 WIB"));
    }

    @Test
    public void weatherAPIMapper() {
        JSONObject apiDummy = new JSONObject("{\"coord\":{\"lon\":106.83,\"lat\":-6.37},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"sedikit mendung\",\"icon\":\"02d\"}],\"base\":\"stations\",\"main\":{\"temp\":305.15,\"pressure\":1009,\"humidity\":59,\"temp_min\":305.15,\"temp_max\":305.15},\"visibility\":8000,\"wind\":{\"speed\":4.1,\"deg\":50},\"clouds\":{\"all\":20},\"dt\":1525248000,\"sys\":{\"type\":1,\"id\":8043,\"message\":0.0073,\"country\":\"ID\",\"sunrise\":1525215195,\"sunset\":1525257960},\"id\":1645524,\"name\":\"Depok\",\"cod\":200}\n");
        Weather weather = new Weather();
        String[] resultArray = weather.mapWeatherAPItoArray(apiDummy);
        assertTrue(resultArray[0].equals("0.0.1"));
        assertTrue(Arrays.toString(resultArray).equals("[0.0.1, 1525248000, 59, 1009, 32, 32, 32, Depok, 05:53:15 WIB, 17:46:00 WIB, sedikit mendung, 50, 4]"));
        assertTrue(weather.mapWeathertoString(resultArray).equals("Cuaca Depok\n" +
                "Waktu : Wed May 02 15:00:00 WIB 2018\n" +
                "Kelembaban : 59%\n" +
                "Tekanan : 1009 hPa\n" +
                "Temperatur (sekarang,max,min) : 32°C,32°C,32°C\n" +
                "Waktu Matahari terbit : 05:53:15 WIB\n" +
                "Waktu Matahari terbenam : 17:46:00 WIB\n" +
                "Kondisi cuaca : sedikit mendung\n" +
                "Angin : 4 Kmh, 50°"));

        //Selalu reset log sebelum di tes
        JSONObject apiDummy2 = new JSONObject("{\"coord\":{\"lon\":106.83,\"lat\":-6.37},\"weather\":[{\"id\":801,\"main\":\"Clouds\",\"description\":\"sedikit mendung\",\"icon\":\"02d\"}],\"base\":\"stations\",\"main\":{\"temp\":305.15,\"pressure\":1009,\"humidity\":59,\"temp_min\":305.15,\"temp_max\":305.15},\"visibility\":8000,\"wind\":{\"speed\":4.1},\"clouds\":{\"all\":20},\"dt\":1525248000,\"sys\":{\"type\":1,\"id\":8043,\"message\":0.0073,\"country\":\"ID\",\"sunrise\":1525215195,\"sunset\":1525257960},\"id\":1645524,\"name\":\"Depok\",\"cod\":200}\n");
        String[] resultArray2 = weather.mapWeatherAPItoArray(apiDummy2);
        assertTrue(resultArray2[0].equals("0.0.1"));
        assertTrue(Arrays.toString(resultArray2).equals("[0.0.1, 1525248000, 59, 1009, 32, 32, 32, Depok, 05:53:15 WIB, 17:46:00 WIB, sedikit mendung, null, 4]"));
        assertTrue(weather.mapWeathertoString(resultArray2).equals("Cuaca Depok\n" +
                "Waktu : Wed May 02 15:00:00 WIB 2018\n" +
                "Kelembaban : 59%\n" +
                "Tekanan : 1009 hPa\n" +
                "Temperatur (sekarang,max,min) : 32°C,32°C,32°C\n" +
                "Waktu Matahari terbit : 05:53:15 WIB\n" +
                "Waktu Matahari terbenam : 17:46:00 WIB\n" +
                "Kondisi cuaca : sedikit mendung\n" +
                "Angin : 4 Kmh"));

    }

    @Test
    public void jsonTest() {
        Weather objek = new Weather();
        String test = "https://api.4stats.io/board/g";
        String testFail = "https://4stats.io";
        String testFail2 = "https://4stats.i";
        String testFail3 = "https://www.google.com/teapot";
        try {
            assertFalse(objek.makeCall(test) == null);
            assertTrue(objek.makeCall(testFail) == null);
            assertTrue(objek.makeCall(testFail2) == null);
            assertTrue(objek.makeCall(testFail3) == null);
        } catch (Exception e) {
            fail("Terjadi kesalahan pada pengambilan resource");
        }

    }

    @Test
    public void weatherFetcherTest() {
        Weather weather = new Weather();
        assertTrue(weather.fetchWeather(0, null, "https://api.openweathermap.org/data/2.5/").equalsIgnoreCase("Opsi salah"));
        assertTrue(weather.fetchWeather(0, "weather", "http://localhost/").equalsIgnoreCase("API Bermasalah"));

    }
    @Test
    public void failSafe(){
        Weather weather = new Weather();
        JSONObject apiDummy = new JSONObject("{\"string\": \"Hello World\"}");
        assertTrue(weather.failSafeAPI(apiDummy,"string").equals("Hello World"));
    }

}

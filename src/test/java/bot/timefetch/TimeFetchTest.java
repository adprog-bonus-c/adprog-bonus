package bot.timefetch;

import bot.AmazingBot;
import bot.Handlerable;
import bot.timefetch.TimeFetch;

import java.text.ParseException;
import java.util.Date;
import java.util.TimeZone;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TimeFetchTest {

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        TimeFetch timefetch = new TimeFetch();

        // Tes 1: "/waktu" -> True

        /* Untuk mengerti ini, anda harus reverse-engineering class.
         Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
         objek yang ingin reverse-engineer lalu ctrl+alt+D
         Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
         Objek Update ada satu kolumn yang ingin kita yaitu column `message`
         Column `mssage` menampung objek Message yang punya dua atribut
         - text yaitu '`echo hi!`
         - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.
         */

        String entry = "{\"message\":{\"text\":\"/waktu\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(timefetch.checkIfForMe(upd));

        // Tes 2: "/waktu hehe" -> True

        entry = "{\"message\":{\"text\":\"/waktu hehe\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(timefetch.checkIfForMe(upd));

        // Tes 3: "waktu" -> False

        entry = "{\"message\":{\"text\":\"waktu\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!timefetch.checkIfForMe(upd));

        // Tes 4: "/help waktu" -> True

        entry = "{\"message\":{\"text\":\"/help waktu\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(timefetch.checkIfForMe(upd));

    }

    @Test
    public void validateProcessMessage() {

        /* Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
         Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
         Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
         dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
         package yang sudah ada dan masuk ke testingLog. String ini
         yang bisa anda periksa kebenarannya
         */

        AmazingBot bot = new AmazingBot(true);
        TimeFetch echo = new TimeFetch();

        // Tes 1 : "/waktu | di-reply dengan menunjukan waktu sekarang ke chatID yang sama
        String entry = "{\"message\":{\"text\":\"/waktu\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Waktu ditampilkan
        assertTrue(bot.testingLog.get(0).contains("Waktu sekarang"));
        // Timezone yang digunakan benar
        assertTrue(bot.testingLog.get(0).contains("WIB"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 2 : "/help waktu | di-reply dengan menunjukan cara penggunaan /waktu
        entry = "{\"message\":{\"text\":\"/help waktu\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Help ditampilkan
        assertTrue(bot.testingLog.get(0).contains("Format: /waktu\n"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 3 : "/waktu hehe" dibalas dengan error karena argumen
        entry = "{\"message\":{\"text\":\"/waktu hehe\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan error yang berupa pemberitahuan bahwa tidak perlu argumen
        assertTrue(bot.testingLog.get(0).contains("text='Argumen /waktu tidak butuh argumen"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
    }

    @Test
    public void testIfServerError() {
        /* Test ini berisi tentang jika menggunakan host
            yang tidak dapat diambil datanya.
         */
        boolean FORMAT_IN_DATE = true;
        String text = TimeFetcher.getTime("123", FORMAT_IN_DATE);
        assertTrue(text.equals("Unknown Hosts"));
    }

    @Test
    public void testIfConvertTimeZoneWorks() {
        /* Test ini mengecek correctness dari convertToWIB
         *
         */
        try {
            DateFormat formatter = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            Date foreign = formatter.parse("Thu Apr 26 00:14:45 JST 2018");
            String text = TimeFetcher.convertToWIB(foreign);
            assertTrue(text.equals("Wed Apr 25 22:14:45 WIB 2018"));
        }catch (ParseException e){
            fail("Terjadi kesalahan pada konversi waktu");
            e.printStackTrace();
        }
    }


}

package bot;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class UtilTest {
    @Test
    public void jsonTest() {
        String test = "https://api.4stats.io/board/g";
        String testFail = "https://4stats.io";
        String testFail2 = "https://4stats.i";
        String testFail3 = "https://www.google.com/teapot";
        try {
            assertFalse(Util.makeCall(test) == null);
            assertTrue(Util.makeCall(testFail) == null);
            assertTrue(Util.makeCall(testFail2) == null);
            assertTrue(Util.makeCall(testFail3) == null);
        } catch (Exception e) {
            fail("Terjadi kesalahan pada pengambilan resource");
        }

    }
}

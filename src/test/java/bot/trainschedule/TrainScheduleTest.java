package bot.trainschedule;

import bot.AmazingBot;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class TrainScheduleTest {

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        TrainSchedule trainSchedule = new TrainSchedule();

        // Tes 1: "/kereta" -> True

        /* Untuk mengerti ini, anda harus reverse-engineering class.
         Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
         objek yang ingin reverse-engineer lalu ctrl+alt+D
         Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
         Objek Update ada satu kolumn yang ingin kita yaitu column `message`
         Column `mssage` menampung objek Message yang punya dua atribut
         - text yaitu '`echo hi!`
         - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.
         */

        String entry = "{\"message\":{\"text\":\"/kereta\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(trainSchedule.checkIfForMe(upd));

        // Tes 2: "/kereta stasiun depok - stasiun bogor" -> True

        entry = "{\"message\":{\"text\":\"/kereta stasiun depok - stasiun bogor\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(trainSchedule.checkIfForMe(upd));

        // Tes 3: "kereta" -> False

        entry = "{\"message\":{\"text\":\"kereta\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!trainSchedule.checkIfForMe(upd));

        // Tes 4: "/help kereta" -> True

        entry = "{\"message\":{\"text\":\"/help kereta\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(trainSchedule.checkIfForMe(upd));

    }

    @Test
    public void validateProcessMessageInitial() {

        /* Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
         Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
         Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
         dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
         package yang sudah ada dan masuk ke testingLog. String ini
         yang bisa anda periksa kebenarannya
         */

        AmazingBot bot = new AmazingBot(true);
        TrainSchedule echo = new TrainSchedule();

        // Tes 1 : "/kereta | di-reply dengan menunjukan error message ke chatID yang sama
        String entry = "{\"message\":{\"text\":\"/kereta\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Waktu ditampilkan
        assertTrue(bot.testingLog.get(0).contains("/kereta membutuhkan argumen <stasiun awal> dan <stasiun tujuan>"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 2 : "/help kereta | di-reply dengan menunjukan cara penggunaan /kereta
        entry = "{\"message\":{\"text\":\"/help kereta\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Help ditampilkan
        assertTrue(bot.testingLog.get(0).contains("Format: /kereta"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 3 : "/kereta stasiun depok" dibalas dengan error karena argumen
        entry = "{\"message\":{\"text\":\"/kereta stasiun depok\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan error yang berupa pemberitahuan bahwa tidak perlu argumen
        assertTrue(bot.testingLog.get(0).contains("text='Input stasiun tidak sesuai"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

    }

    @Test
    public void validateProcessMessageWhenSuccess() {
        /* Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
         Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
         Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
         dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
         package yang sudah ada dan masuk ke testingLog. String ini
         yang bisa anda periksa kebenarannya
         */

        AmazingBot bot = new AmazingBot(true);
        TrainSchedule echo = new TrainSchedule();

        // Tes 4 : "/kereta stasiun depok - stasiun bogor" dibalas dengan rincian jadwal
        String entry = "{\"message\":{\"text\":\"/kereta stasiun depok - stasiun bogor\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan error yang berupa pemberitahuan bahwa tidak perlu argumen
        assertTrue(bot.testingLog.get(0).contains("Keberangkatan dari"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
        System.out.println(bot.testingLog.get(0).toString());

        // Tes 5 : "/kereta stasiun depok-stasiun bogor" dibalas dengan rincian jadwal
        entry = "{\"message\":{\"text\":\"/kereta stasiun depok-stasiun bogor\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan error yang berupa pemberitahuan bahwa tidak perlu argumen
        assertTrue(bot.testingLog.get(0).contains("Keberangkatan dari"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
    }
}

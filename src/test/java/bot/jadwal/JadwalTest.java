package bot.jadwal;

import bot.AmazingBot;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class JadwalTest {
    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        Jadwal jadwal = new Jadwal();

        // Tes 1: "/jadwal" -> True

        //Untuk mengerti ini, anda harus reverse-engineering class.
        //Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
        //objek yang ingin reverse-engineer lalu ctrl+alt+D
        // Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
        // Objek Update ada satu kolumn yang ingin kita yaitu column `message`
        // Column `mssage` menampung objek Message yang punya dua atribut
        // - text yaitu '`echo hi!`
        // - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.

        String entry = "{\"message\":{\"text\":\"/jadwal\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(jadwal.checkIfForMe(upd));

        // Tes 2: "/help jadwal" -> True

        entry = "{\"message\":{\"text\":\"/help jadwal\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(jadwal.checkIfForMe(upd));


        entry = "{\"message\":{\"text\":\"/echo\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!jadwal.checkIfForMe(upd));
    }

    @Test
    public void validateProcessMessage() {

        // Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
        // Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
        // Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
        // dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
        // package yang sudah ada dan masuk ke testingLog. String ini
        // yang bisa anda periksa kebenarannya
        AmazingBot bot = new AmazingBot(true);
        Jadwal jadwal = new Jadwal();

        // Tes 1 : "\help jadwal jadwal di-reply dengan instruksi jadwal
        String entry = "{\"message\":{\"text\":\"/help jadwal\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        assertTrue(bot.testingLog.get(0).contains("text='Format: /jadwal <npm>"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));


        // Tes 2 : "\jadwal di-reply dengan pesan salah parameter
        entry = "{\"message\":{\"text\":\"/jadwal\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        assertTrue(bot.testingLog.get(0).contains("text='Argumen npm membutuhkan satu" +
                " argumen tambahan yaitu NPM'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 4 : NPM bukan numeric

        entry = "{\"message\":{\"text\":\"/jadwal I6O688I67O\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        assertTrue(bot.testingLog.get(0).contains("text='I6O688I67O bukanlah NPM yang valid'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 5 : NPM non-existent

        entry = "{\"message\":{\"text\":\"/jadwal 1606881671\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        assertTrue(bot.testingLog.get(0).contains("text='Beep bop. Bot mengalami kesulitan dalam memproses NPM." +
                " Ini mungkin terjadi karena"
                + " kesalahan teknis atau NPM anda yang salah. Mohon mencoba lagi.'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));


        entry = "{\"message\":{\"text\":\"/jadwal I6O688I67O\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        assertTrue(bot.testingLog.get(0).contains("text='I6O688I67O bukanlah NPM yang valid'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 6 : NPM valid

        entry = "{\"message\":{\"text\":\"/jadwal 1606881670\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        jadwal.processMessage(upd,bot);
        // Hanya ada dua pesan
        assertTrue(bot.testingLog.size()==2);
        assertTrue(bot.testingLog.get(0).contains("text=" +
                "'Jadwal untuk NPM (1606881670) sedang diproses. Ini akan membutuhkan " +
                "beberapa detik untuk diproses."));
        assertTrue(bot.testingLog.get(1).contains("text='~~Senin~~"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
        assertTrue(bot.testingLog.get(1).contains("chatId='10'"));
    }

}

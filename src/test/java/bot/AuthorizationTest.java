package bot;

import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class AuthorizationTest {

    @Test
    public void testAccessTokenIsConsistent(){
        try {
            String token1 = Authorization.getAccessCode();
            assertTrue(token1 != null);
            String token2 = Authorization.getAccessCode();
            assertTrue(token1 == token2);
        } catch (Exception e) {
            e.printStackTrace();
            fail("Terjadi error saat pengambilan token. Mungkin karena anda belum menyetel username dan password" +
                    "di environment variable");
        }
    }

    @Test
    public void testClientId(){
        String token1 = Authorization.getClientId();
        assertNotNull(token1);

    }

    @Test
    public void testUsernameAndAccessTokenTelegramIsExist() {
        assertNotNull(Authorization.getUsernameTelegram());
        assertNotNull(Authorization.getAccessTokenTelegram());
    }
}

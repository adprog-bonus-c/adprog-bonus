package bot.help;

import bot.AmazingBot;
import bot.Handlerable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class HelpTest {

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        Help help = new Help();

        // Tes 1: "/help" -> True

        /* Untuk mengerti ini, anda harus reverse-engineering class.
         Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
         objek yang ingin reverse-engineer lalu ctrl+alt+D
         Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
         Objek Update ada satu kolumn yang ingin kita yaitu column `message`
         Column `mssage` menampung objek Message yang punya dua atribut
         - text yaitu '`echo hi!`
         - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.
         */

        String entry = "{\"message\":{\"text\":\"/help\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(help.checkIfForMe(upd));
        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 2: "        /heLP    " -> True

        entry = "{\"message\":{\"text\":\"        /help    \",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);

        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(help.checkIfForMe(upd));

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 3: "help" -> False

        entry = "{\"message\":{\"text\":\"help\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 4: "\help" -> False

        entry = "{\"message\":{\"text\":\"\\\\help\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));


        //Selalu reset log sebelum di tes
        bot.testingLog.clear();


        // Tes 5: "/help asasasas" -> True

        entry = "{\"message\":{\"text\":\"/help asasasas\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(help.checkIfForMe(upd));

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();


        // Tes 6: "/help announcement" -> True

        entry = "{\"message\":{\"text\":\"/help announcement\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 7: "/help echo" -> True

        entry = "{\"message\":{\"text\":\"/help echo\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 8: "/help jadwal" -> True

        entry = "{\"message\":{\"text\":\"/help jadwal\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));


        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        // Tes 9: "/help waktu" -> True

        entry = "{\"message\":{\"text\":\"/help waktu\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!help.checkIfForMe(upd));
    }

    @Test
    public void validateProcessMessage() {

        /* Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
         Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
         Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
         dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
         package yang sudah ada dan masuk ke testingLog. String ini
         yang bisa anda periksa kebenarannya
         */

        AmazingBot bot = new AmazingBot(true);
        Help help = new Help();

        // Tes 1 : "/help" -> bot akan mengirimkan pesan yang berisi petunjuk pemakaian
        String entry = "{\"message\":{\"text\":\"/help\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        help.processMessage(upd,bot);
        // Setiap Feature/fungsionalitas terdapat di pesan
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // detail announcement
        assertTrue(bot.testingLog.get(0).contains("/announcement bot akan menampilkan pengumuman di scele.cs.ui.ac.id"));
        // detail echo
        assertTrue(bot.testingLog.get(0).contains("/echo bot akan mengirimkan kembali pesan yg dikirim"));
        // detail announcement
        assertTrue(bot.testingLog.get(0).contains("/help menampilkan bantuan"));
        // detail waktu
        assertTrue(bot.testingLog.get(0).contains("/waktu menampilkan waktu"));
        // detail waktu
        assertTrue(bot.testingLog.get(0).contains("/cuaca untuk menampilkan cuaca di depok dan salemba"));

        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 2 : "/help asasas" dibalas dengan error karena argumen
        entry = "{\"message\":{\"text\":\"/help asasas\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        help.processMessage(upd,bot);
        //
        assertTrue(bot.testingLog.size()==0);

    }

}

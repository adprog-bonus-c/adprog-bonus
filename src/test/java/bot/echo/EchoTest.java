package bot.echo;

import bot.AmazingBot;
import bot.Handlerable;
import bot.echo.Echo;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;


public class EchoTest {

    public AmazingBot bot;
    public Handlerable echo;

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        Echo echo = new Echo();

        // Tes 1: "/echo hi!" -> True

        //Untuk mengerti ini, anda harus reverse-engineering class.
        //Di Intellij, anda bisa membuka kelas tersebut dengan cara menaruh cursor ke
        //objek yang ingin reverse-engineer lalu ctrl+alt+D
        // Pada dasarnya kita ingin mengkonversi JSON jadi objek Update
        // Objek Update ada satu kolumn yang ingin kita yaitu column `message`
        // Column `mssage` menampung objek Message yang punya dua atribut
        // - text yaitu '`echo hi!`
        // - chat yang menampung objek Chat yang mempunyai atribut `id` yaitu 10.

        String entry = "{\"message\":{\"text\":\"/echo hi!\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(echo.checkIfForMe(upd));

        // Tes 2: "/help echo" -> True

        entry = "{\"message\":{\"text\":\"/help echo\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(echo.checkIfForMe(upd));

        // Tes 3: "/echo" -> True
        // Memang ini argumenya tidak valid, tetapi dia baru di "Exception Handling" oleh processMessage
        // bukan ditolak oleh checkIfForMe

        entry = "{\"message\":{\"text\":\"/echo\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(echo.checkIfForMe(upd));

        // Tes 4:"/ecyo /echo" -> False

        entry = "{\"message\":{\"text\":\"/ecyo /echo\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!echo.checkIfForMe(upd));
    }

    @Test
    public void validateProcessMessage() {

        // Ini adalah contoh testCase yang mengecek pesan yang dikirim oleh bot
        // Ketika AmazingBot di set ke mode = true, maka bot dibuat dalam keadaan debugging
        // Ketika debugging, pesan tidak dikirim ke Telegram tetapi objek pesan itu
        // dikonveri menjadi String menggunakan toString() yang diimplementasikan dari
        // package yang sudah ada dan masuk ke testingLog. String ini
        // yang bisa anda periksa kebenarannya
        AmazingBot bot = new AmazingBot(true);
        Echo echo = new Echo();

        // Tes 1 : "\echo hi!| di-reply dengan "hi!" ke chatID yang sama
        String entry = "{\"message\":{\"text\":\"/echo hi!\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan itu adalah 'Hi!'
        assertTrue(bot.testingLog.get(0).contains("text='hi!'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 2 : "\help echo" dibalas dengan instruksi echo yang tepat ke chatID yang sama
        entry = "{\"message\":{\"text\":\"/help echo\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan itu ada intruksi help
        assertTrue(bot.testingLog.get(0).contains("text='Format: /echo <argumen>\n"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 3 : "\echo" dibalas dengan instruksi bahwa kurang argumen
        entry = "{\"message\":{\"text\":\"/echo\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }

        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan itu ada intruksi help
        assertTrue(bot.testingLog.get(0).contains("text='Argumen /echo gagal. Dia membutuhkan setidaknya satu argumen'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 3 : "/echo wow bisa lebih dari satu kata" dibalas dengan "wow bisa lebih dari satu kata"
        entry = "{\"message\":{\"text\":\"/echo wow bisa lebih dari satu kata\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }

        echo.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertTrue(bot.testingLog.size()==1);
        // Pesan itu ada intruksi help
        assertTrue(bot.testingLog.get(0).contains("text='wow bisa lebih dari satu kata'"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
    }


}

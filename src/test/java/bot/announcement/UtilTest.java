package bot.announcement;

import org.junit.Test;

import static org.junit.Assert.*;

public class UtilTest {

    @Test
    public void unescapeHTMLTest() {
        assertEquals("&nbsp", Util.unescapeHTML("&nbsp"));
        assertNotEquals("", Util.unescapeHTML("&nbsp"));
    }

    @Test
    public void removeHtmlTagTest() {
        assertEquals(Util.removeHtmlTag("<h1>ARGA<h1>"), "ARGA");
        assertNotEquals(Util.removeHtmlTag("<h1>ARGA<h1>"), "");
    }
}
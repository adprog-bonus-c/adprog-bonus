package bot.announcement;

import bot.AmazingBot;
import bot.Handlerable;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.telegram.telegrambots.api.objects.Update;

import static org.junit.Assert.*;

public class AnnouncementTest {
    public AmazingBot bot;
    public Handlerable handlerable;

    public Update getUpdateGivenJson(String update){
        ObjectMapper mapper = new ObjectMapper();
        try {
            return  mapper.readValue(update, Update.class);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void validateCheckIfForMe() {
        AmazingBot bot = new AmazingBot(true);
        Announcement announcement = new Announcement();

        // Tes 1: "\announcement 5" -> True

        String entry = "{\"message\":{\"text\":\"/announcement 5\",\"chat\":{\"id\":\"10\"}}}";

        Update upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(announcement.checkIfForMe(upd));

        // Tes 2: "\help announcement" -> True

        entry = "{\"message\":{\"text\":\"/help announcement\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(announcement.checkIfForMe(upd));

        // Tes 3:"\anouncement \1" -> False

        entry = "{\"message\":{\"text\":\"/anouncement \\\\1\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertTrue(!announcement.checkIfForMe(upd));

        // Tes 4:update tidak berisi pesan
        entry = "{\"message\":{\"text\":\"\",\"chat\":{\"id\":\"10\"}}}";

        upd = getUpdateGivenJson(entry);
        if(upd == null){
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        assertFalse(announcement.checkIfForMe(upd));
    }

    @Test
    public void validateProcessMessage() {
        AmazingBot bot = new AmazingBot(true);
        Announcement announcement = new Announcement();

        // Tes 1 : "\announcement| di-reply ke chatID yang sama
        String entry = "{\"message\":{\"text\":\"/announcement\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        Update upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        announcement.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertEquals(1, bot.testingLog.size());
        // Pesan itu adalah 'Pengumuman Akademis:!'
        assertTrue(bot.testingLog.get(0).contains("text='Pengumuman Akademis:"));
        // Dikirim ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));

        // Tes 2 : "\help announcement" dibalas dengan instruksi echo yang tepat ke chatID yang sama
        entry = "{\"message\":{\"text\":\"/help announcement\",\"chat\":{\"id\":\"10\"}}}";

        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        announcement.processMessage(upd,bot);
        // Hanya ada satu pesan
        assertEquals(1, bot.testingLog.size());
        // Pesan itu ada intruksi help
        assertTrue(bot.testingLog.get(0).contains("text='Format: /announcement <optional: jumlah yang ditampilkan min 1, max 5, dan default 1>\n"));
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));


        // Tes 3: "\announcement 4"
        entry = "{\"message\":{\"text\":\"/announcement 4\",\"chat\":{\"id\":\"10\"}}}";
        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        announcement.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertEquals(1, bot.testingLog.size());
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
        // Pesan itu adalah 'Pengumuman Akademis:!'
        assertTrue(bot.testingLog.get(0).contains("text='Pengumuman Akademis:"));

        // Tes 4: "\announcement 10"
        entry = "{\"message\":{\"text\":\"/announcement 10\",\"chat\":{\"id\":\"10\"}}}";
        //Selalu reset log sebelum di tes
        bot.testingLog.clear();

        upd = getUpdateGivenJson(entry);
        if (upd == null) {
            fail("Terjadi kesalahan pada konversi JSON -> Update");
        }
        announcement.processMessage(upd, bot);
        // Hanya ada satu pesan
        assertEquals(1, bot.testingLog.size());
        // Dikirm ke chatId yang tepat
        assertTrue(bot.testingLog.get(0).contains("chatId='10'"));
        // Pesan itu adalah 'Pengumuman Akademis:!'
        assertTrue(bot.testingLog.get(0).contains("text='Maaf, angka yang kamu masukkan harus diantara 1 dan 5 inklusif.'"));
    }

}
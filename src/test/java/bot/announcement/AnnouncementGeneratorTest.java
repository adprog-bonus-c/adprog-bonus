package bot.announcement;

import org.junit.Test;

import static org.junit.Assert.*;

public class AnnouncementGeneratorTest {

    @Test
    public void generateAnnouncementTest() {
        AnnouncementGenerator announcementGenerator = new AnnouncementGenerator();
        assertTrue(announcementGenerator.generateAnnouncement(1).contains("Pengumuman Akademis:"));
        assertNotEquals("", announcementGenerator.generateAnnouncement(1));
    }
}
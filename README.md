# Grup Bonus Adprog

- Arga Ghulam Ahmad
- Kerenza Doxolodeo
- Made Wira Dhanar Santika
- Geraldo William

## Cara pemakaian

- Maven digunakan untuk handling package secara otomatis
- Setting environment variable di Heroku dan di Gitlab
 + ACCESS_TOKEN_TELEGRAM dan USERNAME_TELEGRAM bisa didapatkan dari Botfather di Telegram jika ingin membuat app baru
 + HEROKU_APIKEY , HEROKU_APPNAME , HEROKU_APP_HOST dibutuhkan untuk proses deploy. HEROKU_APIKEY dapat didapatkan dari dashboard Heroku. Tidak dibutuhkan untuk ditambahkan ke environment variable Heroku.
 + SSO_NPM , SSO_PASSWORD , SSO_USERNAME dibutuhkan untuk mengakses API CS UI
 + WEATHER_TOKEN dibutuhkan untuk mengakses API OpenWeather
 + GOOGLE_MAPS_API dibutuhkan untuk mengakses jadwal kereta via Google Maps


## Detail Bot:

[![coverage report](https://gitlab.com/adprog-bonus-c/adprog-bonus/badges/master/coverage.svg)]((https://gitlab.com/adprog-bonus-c/adprog-bonus/commits/master)
)

### Produksi

Nama bot di Telegram: UncleAdprog

[![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/Uncle_Adprog_Bot)

### Development

Nama bot versi development di Telegram: UncleAdprogDev

[![Telegram](http://trellobot.doomdns.org/telegrambadge.svg)](https://telegram.me/Uncle_AdprogDev)

### Package

Chatbot berbasiskan Java ini memakai *package*
[TelegramBots](https://github.com/rubenlagus/TelegramBots).

## Daftar fitur 

- [x] Announcement - Arga Ghulam Ahmad
- [x] Jadwal - Kerenza Doxoldeo
- [x] Waktu - Dhanar Santika
- [x] Weather - Geraldo William
- [x] Kereta - Dhanar Santika

## Cara kerja Bot 

Bot ini menggunakan design pattern chain of command dengan cara berikut.

Ketika sebuah pesan masuk, bot akan meng-iterate daftar kelas-kelas yang meng support fitur tersebut.

Bot akan memanggil fitur kelas pertama dengan memanggil implementasi kelas tersebut dari `checkIfForMe()`. Jika kelas fitur itu menganggap pesan itu bukan tanggung-jawab fitur dia, kelas itu akan meng-return false. Loop lalu akan menuju ke fitur kelas selanjutnya dan mengecek respon dari `checkIfForMe()`

Jika Bot itu menganggap pesan itu tanggung-jawab dia, pemanggilan `checkIfForMe()` dari kelas itu akan meng-return true. Dengan meng-return true, pesan itu lalu di claim oleh kelas fitur itu. Bot akan memanggil implementasi dari `processMessage()` dengan memberikan pesan yang diklaimnya sebagai salah satu parameternya. Method itu akan memproses pesan itu dan mengirim pesan dari sana. Karena pesan itu sudah diclaim, bot akan berhenti memanggil implementasi `checkIfForMe()` fitur selanjutnya untuk pesan itu.

Jika pesan itu tidak ada yang meng-klaim, maka bot akan mengirim pesan kepada pengguna yang mengatakan bahwa pesan tersebut invalid.

## Cara kerja menambahkan fitur

Buat sub-package di bot. Buat sebuah kelas yang digunakan untuk memproses fitur. Harus ada kelas yang mengimplementasi interface `Handlerable` yang memiliki kontrak `checkIfForMe` dan `processMessage` (ini nanti method yang dipakai oleh loop untuk mengecek pesan.)

NOTE : Di dokumentasi TelegramBot, anda bisa mengirim pesan menggunakan execute(). Gunakan send() daripada execute(). Send adalah wrapper execute() yang dioptimisasi untuk proses Testing.

Ubah constructor AmazingBot.java agar kelas fitur terdaftar di ArrayList.

## Cara kerja Testing

Contoh kerja testing dapat dilihat di test\java\bot\echo\bot.echo.EchoTest.java, tetapi pada dasarnya ada beberapa hal yang perlu diperhitungkan.

1. AmazingBot (dan bot yang anda buat yang meng-extend AbstractBot) untuk keperluan testing harus di inti dengan mode = True. Ketika anda meng init mode = True, object yang anda coba send() akan masuk ke arrayList testingLog untuk anda inspeksi, sementara jika menggunakan mode = False, object yang anda coba send() akan dikirim langsung.

2. Objek Update berguna untuk mengsimulasi transaksi pesan dari server Telegram. Anda dapat mengkonstruksi Update dengan mengkonversi JSON -> Update menggunakan ObjectMapper

## Reminder

- Kode kamu harus mencegah racing condition. Selalu asumsikan kode kamu akan dipakai dalam konteks multi-threading
* TelegramBots bersifat sinkronus jadi kalau fitur kamu lama, bakal mandek. Salah satu alasan program kamu bisa mandek adalah melakukan scrapping atau API CS UI. (Satu pemanggilan API CS UI kurang lebih dua detik) Cache ketika kamu bisa, terutama data yang jarang berubah.

## Resources

- Dokumentasi Library Bot Telegram : https://github.com/rubenlagus/TelegramBots
- Tutorial Bot Telegram menggunakan Library TelegramBots : https://legacy.gitbook.com/book/monsterdeveloper/writing-telegram-bots-on-java/details (Bab 1, Bab 5, Bab 6 menarik)
